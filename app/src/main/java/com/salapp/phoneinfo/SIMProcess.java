package com.salapp.phoneinfo;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.salapp.phoneinfo.permission.AndroidPermission;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

import java.util.ArrayList;
import java.util.List;

public class SIMProcess {
    private List<ListDeviceInfo> lstInfoSIM;
    private Context context;
    private Activity activity;

    public SIMProcess(SIMFragment activity) {
        this.context = activity.getActivity();
        this.activity = activity.getActivity();

    }


    public List<ListDeviceInfo> getSIMProcess() {
        lstInfoSIM = new ArrayList<>();
        getSIMInfo();
        getSimCountryIso();
        getSimOperator();
        getSimOperatorName();
        getPhoneNumber();
        getSimSerialNumber();
        getSimState();
        getSubscriberId();
        getPhoneType();
        getVoiceMailNumber();
        getNetworkType();
        return lstInfoSIM;
    }

    private void getSIMInfo() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = null;

        mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = mTelephonyMgr.getDeviceId();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("IMEI");
        deviceInfo.setValue(imei);
        lstInfoSIM.add(deviceInfo);
    }

    private void getSimCountryIso() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String simCountryIso = mTelephonyMgr.getSimCountryIso();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("SIM Country ISO");
        deviceInfo.setValue(simCountryIso);
        lstInfoSIM.add(deviceInfo);
    }

    private void getSimOperator() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String simOperator = mTelephonyMgr.getSimOperator();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("SIM Operator");
        deviceInfo.setValue(simOperator);
        lstInfoSIM.add(deviceInfo);
    }

    private void getSimOperatorName() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String simOperatorName = mTelephonyMgr.getSimOperatorName();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("SIM Operator Name");
        deviceInfo.setValue(simOperatorName);
        lstInfoSIM.add(deviceInfo);
    }

    public void getSimSerialNumber() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String simSerialNumber = mTelephonyMgr.getSimSerialNumber();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("SIM Serial Number");
        deviceInfo.setValue(simSerialNumber);
        lstInfoSIM.add(deviceInfo);
    }

    public void getSimState() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int state = mTelephonyMgr.getSimState();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("SIM State");

        switch (state) {
            case 0:
                deviceInfo.setValue("SIM_STATE_UNKNOWN");
                break;
            case 1:
                deviceInfo.setValue("SIM_STATE_ABSENT");
                break;
            case 2:
                deviceInfo.setValue("SIM_STATE_PIN_REQUIRED");
                break;
            case 3:
                deviceInfo.setValue("SIM_STATE_PUK_REQUIRED");
                break;
            case 4:
                deviceInfo.setValue("SIM_STATE_NETWORK_LOCKED");
                break;
            case 5:
                deviceInfo.setValue("SIM_STATE_READY");
                break;
            default:
        }
        lstInfoSIM.add(deviceInfo);
    }

    public void getSubscriberId() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String simSubscriberId = mTelephonyMgr.getSubscriberId();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Subscriber ID");
        deviceInfo.setValue(simSubscriberId);
        lstInfoSIM.add(deviceInfo);
    }

    public void getPhoneType() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int phoneType = mTelephonyMgr.getPhoneType();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Phone Type");

        switch (phoneType) {
            case 0:
                deviceInfo.setValue("PHONE_TYPE_NONE");
                break;
            case 1:
                deviceInfo.setValue("PHONE_TYPE_GSM");
                break;
            case 2:
                deviceInfo.setValue("PHONE_TYPE_CDMA");
                break;
            case 3:
                deviceInfo.setValue("PHONE_TYPE_SIP");
                break;
            default:
                deviceInfo.setValue("UNKNOW");
        }
        lstInfoSIM.add(deviceInfo);
    }

    public void getPhoneNumber() {
        try {
            Context context = this.context.getApplicationContext();
            TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String phoneNumber = mTelephonyMgr.getLine1Number();

            ListDeviceInfo deviceInfo = new ListDeviceInfo();
            deviceInfo.setTitle("Phone number");
            deviceInfo.setValue(phoneNumber);
            if (!deviceInfo.getValue().equals("")) {
                lstInfoSIM.add(deviceInfo);
            }
        } catch (Exception e) {
            Log.e("phoneNumber", "Don't exist phone number");
        }
    }

    public void getVoiceMailNumber() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String voiceMailNumber = mTelephonyMgr.getVoiceMailNumber();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Voice Mail number");
        deviceInfo.setValue(voiceMailNumber);
        lstInfoSIM.add(deviceInfo);
    }

    public void getNetworkType() {
        Context context = this.context.getApplicationContext();
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyMgr.getNetworkType();

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Network Type");

        switch (networkType) {
            case 1:
                deviceInfo.setValue("NETWORK_TYPE_GPRS");
                break;
            case 2:
                deviceInfo.setValue("NETWORK_TYPE_EDGE");
                break;
            case 3:
                deviceInfo.setValue("NETWORK_TYPE_UMTS");
                break;
            case 4:
                deviceInfo.setValue("NETWORK_TYPE_HSDPA");
                break;
            case 5:
                deviceInfo.setValue("NETWORK_TYPE_HSUPA");
                break;
            case 6:
                deviceInfo.setValue("#NETWORK_TYPE_HSPA");
                break;
            case 7:
                deviceInfo.setValue("NETWORK_TYPE_CDMA");
                break;
            case 8:
                deviceInfo.setValue("NETWORK_TYPE_EVDO_0");
                break;
            case 9:
                deviceInfo.setValue("NETWORK_TYPE_EVDO_A");
                break;
            case 10:
                deviceInfo.setValue("NETWORK_TYPE_EVDO_B");
                break;
            case 11:
                deviceInfo.setValue("NETWORK_TYPE_1xRTT");
                break;
            case 12:
                deviceInfo.setValue("NETWORK_TYPE_IDEN");
                break;
            case 13:
                deviceInfo.setValue("NETWORK_TYPE_LTE");
                break;
            case 14:
                deviceInfo.setValue("NETWORK_TYPE_EHRPD");
                break;
            case 15:
                deviceInfo.setValue("NETWORK_TYPE_HSPAP");
                break;
            default:
                deviceInfo.setValue("NETWORK_TYPE_UNKNOWN");
        }
        lstInfoSIM.add(deviceInfo);
    }
}