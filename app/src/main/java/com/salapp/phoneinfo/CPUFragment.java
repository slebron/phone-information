package com.salapp.phoneinfo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.android.hardware.process.CPUProcess;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/4/13
 * Time: 10:29 AM
 */
public class CPUFragment extends Fragment {

    private ListView lstAndroidInformation;
    private List<ListDeviceInfo> lstInfoGPU;

    public CPUFragment(){
        lstInfoGPU = new ArrayList<ListDeviceInfo>();
        lstInfoGPU = new CPUProcess(this).lstCPUInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cpu_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstAndroidInformation = (ListView) getView().findViewById(R.id.lstCPUInfo);
        lstAndroidInformation.setAdapter(new CPUAdapter(this));
    }

    private class CPUAdapter extends ArrayAdapter<ListDeviceInfo> {
        Activity context;

        public CPUAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstInfoGPU);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstInfoGPU.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstInfoGPU.get(position).getValue());
            return (view);
        }
    }
}
