package com.salapp.phoneinfo.android.hardware.process;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/14/13
 * Time: 2:12 PM
 */
public class DisplayProcess {
    private List<ListDeviceInfo> listDeviceInfo;
    private Activity activity;

    public DisplayProcess(Fragment activy) {
        this.activity = activy.getActivity();
    }

    public List<ListDeviceInfo> getListDisplayInfo() {
        listDeviceInfo = new ArrayList<ListDeviceInfo>();
        getDisplayInfo();
        getScreenDimesion();
        getScreenSize();
        getScreenDensity();
        getExactDPI();
        return listDeviceInfo;
    }

    public void getPixelDensity() {
        WindowManager windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int density = displayMetrics.densityDpi;

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Pixel Density");
        deviceInfo.setValue(String.valueOf(density));
        listDeviceInfo.add(deviceInfo);
    }

    public void getDisplayInfo() {
        WindowManager windowManager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        int width = 0;
        int height = 0;
        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Resolution");
        if (Build.VERSION.SDK_INT > 12) {
            Point size = new Point();
            display.getSize(size);
            width = size.x;
            height = size.y;
            deviceInfo.setValue(height + " x " + width + " pixels\n");
        } else {
            width = display.getWidth();
            height = display.getHeight();
            deviceInfo.setValue(height + " x " + width + " pixels\n");
        }

        listDeviceInfo.add(deviceInfo);
    }

    public void getScreenDimesion() {
        DisplayMetrics metrics = new DisplayMetrics();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getMetrics(metrics);

        double widthInches = metrics.widthPixels / metrics.xdpi;
        double heightInches = metrics.heightPixels / metrics.ydpi;
        double diagonalInches = Math.sqrt(widthInches * widthInches + heightInches * heightInches);

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Approximate Dimensions");
        String formatDimension = String.format("%.1f\" x %.1f\" (%.1f\" diagonal)\n", widthInches, heightInches, diagonalInches);
        deviceInfo.setValue(formatDimension);

        listDeviceInfo.add(deviceInfo);
    }

    public void getScreenDensity() {
        DisplayMetrics metrics = new DisplayMetrics();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getMetrics(metrics);

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Screen Density");
        String result = String.valueOf(metrics.densityDpi + "dpi (" + metrics.density + "x DIP)\n");
        deviceInfo.setValue(result);
        listDeviceInfo.add(deviceInfo);
    }

    public void getScreenSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getMetrics(metrics);

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Screen Size");
        String result = String.valueOf(metrics.widthPixels + " x " + metrics.heightPixels + " pixels)\n");
        deviceInfo.setValue(result);
        listDeviceInfo.add(deviceInfo);
    }

    public void getExactDPI() {
        DisplayMetrics metrics = new DisplayMetrics();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getMetrics(metrics);

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Exact DPI");
        String result = String.valueOf(metrics.xdpi + " x " + metrics.ydpi + "\n");
        deviceInfo.setValue(result);
        listDeviceInfo.add(deviceInfo);
    }
}
