package com.salapp.phoneinfo.android.hardware.process;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * User: Stainley Lebron
 * Date: 5/12/13
 * Time: 5:23 PM
 */
public class GPUProcess implements GLSurfaceView.Renderer{
    private StringBuffer sb = new StringBuffer();

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        FileUtils fileUtil = new FileUtils();
        String vendor = gl10.glGetString(GL10.GL_VENDOR);
        String render = gl10.glGetString(GL10.GL_RENDERER);
        String version = gl10.glGetString(GL10.GL_VERSION);
        String extension = gl10.glGetString(GL10.GL_EXTENSIONS);
        sb.append(vendor).append("\n").append(render).append("\n").append(version).append("\n").append(extension).append("\n");

        fileUtil.writeToFile(sb.toString());
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
    }

}
