package com.salapp.phoneinfo.android.hardware.process;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import com.salapp.phoneinfo.ProcessFragment;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/14/13
 * Time: 9:26 AM
 */
public class ProcessManager {

    private List<ListDeviceInfo> listDeviceInfo;
    private Activity context;

    public ProcessManager(ProcessFragment fragment) {
        this.context = fragment.getActivity();
    }

    public List<ListDeviceInfo> getAllProcess() {
        listDeviceInfo = new ArrayList<ListDeviceInfo>();
        showProcess();
        return listDeviceInfo;
    }

    public void getNumberProcess(List<ActivityManager.RunningAppProcessInfo> process) {
        int numberProcess = process.size();
        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Count Process Running");
        deviceInfo.setValue(String.valueOf(numberProcess));
        listDeviceInfo.add(deviceInfo);
    }

    public void showProcess() {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> lst = manager.getRunningAppProcesses();

        getNumberProcess(lst);

        ListDeviceInfo deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Process Running");
        StringBuilder sb = new StringBuilder();
        for (ActivityManager.RunningAppProcessInfo m : lst) {
            sb.append(m.processName).append("\n\n");
        }
        deviceInfo.setValue(sb.toString());
        listDeviceInfo.add(deviceInfo);
    }

}
