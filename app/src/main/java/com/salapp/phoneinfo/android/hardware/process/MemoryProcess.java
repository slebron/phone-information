package com.salapp.phoneinfo.android.hardware.process;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.salapp.phoneinfo.MemoryFragment;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/12/13
 * Time: 8:37 PM
 */
public class MemoryProcess {
    private Map<String, String> memoryInfo = new HashMap<String, String>();
    private List<ListDeviceInfo> lstMemoryInfo;
    private Activity context;

    public MemoryProcess(MemoryFragment context) {
        this.context = context.getActivity();
        lstMemoryInfo = new ArrayList<ListDeviceInfo>();
    }

    public List<ListDeviceInfo> getMemoryProcess() {

        return lstMemoryInfo;
    }

    /**
     * Este  metodo extrae la informacion de la memoria.
     *
     * @return lista de memoria
     */

    public List<ListDeviceInfo> getInfoMemory() {
        getMemory();
        Iterator iterator = memoryInfo.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry map = (Map.Entry) iterator.next();
            if (map.getKey().equals("total memory")) {
                ListDeviceInfo cpuInfo = new ListDeviceInfo();
                cpuInfo.setTitle("Total Memory");
                cpuInfo.setValue((String) map.getValue());
                lstMemoryInfo.add(cpuInfo);
            } else if (map.getKey().equals("used memory")) {
                ListDeviceInfo cpuInfo = new ListDeviceInfo();
                cpuInfo.setTitle("Used memory");
                cpuInfo.setValue((String) map.getValue());
                lstMemoryInfo.add(cpuInfo);
            } else if (map.getKey().equals("free memory")) {
                ListDeviceInfo cpuInfo = new ListDeviceInfo();
                cpuInfo.setTitle("Free memory");
                cpuInfo.setValue((String) map.getValue());
                lstMemoryInfo.add(cpuInfo);

            } else if (map.getKey().equals("buffers memory")) {
                ListDeviceInfo cpuInfo = new ListDeviceInfo();
                cpuInfo.setTitle("Buffed Memory");
                cpuInfo.setValue((String) map.getValue());
                lstMemoryInfo.add(cpuInfo);
            }
        }
        return lstMemoryInfo;
    }

    private MemInfo readMemInfo() {
        MemInfo result = new MemInfo();

        String[] lines = Utils.readLines("/proc/meminfo");
        for (String line : lines) {
            String[] fields = line.split(" +");
            if (fields[0].equals("MemTotal:")) {
                result.total = Long.parseLong(fields[1]) * 1024;
            } else if (fields[0].equals("MemFree:")) {
                result.used = Long.parseLong(fields[1]) * 1024;
            } else if (fields[0].equals("Buffers:")) {
                result.buffers = Long.parseLong(fields[1]) * 1024;
            } else {
                break;
            }
        }
        result.free = result.total - result.used;
        return result;
    }

    public void getMemoria() {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo1 = new ActivityManager.MemoryInfo();

        activityManager.getMemoryInfo(memoryInfo1);
        memoryInfo.put("disponible", Utils.prettySize(memoryInfo1.availMem));
        memoryInfo.put("usada", Utils.prettySize(memoryInfo1.threshold));
    }

    public void getTotalRAM() {
        RandomAccessFile reader = null;
        String load = null;
        try {
            reader = new RandomAccessFile("/proc/meminfo", "r");
            load = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            // Streams.close(reader);
        }
        ListDeviceInfo listDeviceInfo = new ListDeviceInfo();
        listDeviceInfo.setTitle("Total RAM");
        listDeviceInfo.setValue(load);
        lstMemoryInfo.add(listDeviceInfo);
    }

    public void getMemory() {
        getMemoria();
        MemInfo memInfo = readMemInfo();
        memoryInfo.put("total memory", Utils.prettySize(memInfo.total));
        memoryInfo.put("used memory", Utils.prettySize(memInfo.used));
        memoryInfo.put("free memory", Utils.prettySize(memInfo.free));
        memoryInfo.put("buffers memory", Utils.prettySize(memInfo.buffers));
    }

    static class MemInfo {
        long total;
        long used;
        long free;
        long buffers;
    }
}
