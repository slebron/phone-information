package com.salapp.phoneinfo.android.hardware.process;

import android.os.Build;
import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/4/13
 * Time: 11:26 AM
 */
public class DeviceProcess {
    private ListDeviceInfo deviceInfo;
    private List<ListDeviceInfo> lstDeviceInfo;

    public List<ListDeviceInfo> getDeviceProcess() {
        lstDeviceInfo = new ArrayList<ListDeviceInfo>();
        getBrand();
        getModel();
        getManufacturer();
        getProduct();
        getUser();
        return lstDeviceInfo;
    }

    private String get(){
        deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Board");
        deviceInfo.setValue(Build.BOARD);
        lstDeviceInfo.add(deviceInfo);
        return Build.BRAND;
    }

    private String getBrand() {
        deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("BRAND");
        deviceInfo.setValue(Build.BRAND);
        lstDeviceInfo.add(deviceInfo);
        return Build.BRAND;
    }

    private String getModel() {
        deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("MODEL");
        deviceInfo.setValue(Build.MODEL);
        lstDeviceInfo.add(deviceInfo);
        return Build.MODEL;
    }

    private String getManufacturer(){
        deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Manufacturer");
        deviceInfo.setValue(Build.MANUFACTURER);
        lstDeviceInfo.add(deviceInfo);
        return Build.MANUFACTURER;
    }

    private void getProduct(){
        deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("Product");
        deviceInfo.setValue(Build.PRODUCT);
        lstDeviceInfo.add(deviceInfo);
    }

    private void getUser(){
        deviceInfo = new ListDeviceInfo();
        deviceInfo.setTitle("User");
        deviceInfo.setValue(Build.USER);
        lstDeviceInfo.add(deviceInfo);
    }

}
