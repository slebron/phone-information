package com.salapp.phoneinfo.android.hardware.process;

import android.app.Activity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.salapp.phoneinfo.CPUFragment;
import com.salapp.phoneinfo.vo.CPU;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/12/13
 * Time: 8:10 PM
 */
public class CPUProcess {
    private List<ListDeviceInfo> listDeviceInfo;
    private Activity context;
    private CPU cpu;

    public CPUProcess(CPUFragment activity) {
        listDeviceInfo = new ArrayList<ListDeviceInfo>();
        this.context = activity.getActivity();
    }


    public List<ListDeviceInfo> lstCPUInfo() {
        getDeviceDetailsAsString(context);
        getProcessor();
        getImplementer();
        getPart();
        getSpeed();
        getCores();
        getModel();
        getHardware();
        getArquitecture();
        getCache();
        getSerial();
        getAddressSize();
        getRevision();
        getFeatures();
        return listDeviceInfo;
    }

    private static int countHardwareCores() {
        int result = 0;
        for (String file : new File("/sys/devices/system/cpu").list()) {
            if (file.matches("cpu[0-9]+")) {
                ++result;
            }
        }
        return result;
    }

    private static int countEnabledCores() {
        int count = 0;
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("/proc/stat"));
            String line;
            while ((line = in.readLine()) != null) {
                if (line.startsWith("cpu") && !line.startsWith("cpu ")) {
                    ++count;
                }
            }
            return count;
        } catch (IOException ex) {
            return -1;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    private static String valueForKey(String[] lines, String key) {
        // If you touch this, test on ARM, MIPS, and x86.
        Pattern p = Pattern.compile("(?i)" + key + "\t*: (.*)");
        for (String line : lines) {
            Matcher m = p.matcher(line);
            if (m.matches()) {
                return m.group(1);
            }
        }
        return null;
    }

    private static int numericValueForKey(String[] lines, String key) {
        String value = valueForKey(lines, key);
        if (value == null) {
            return -1;
        }
        int base = 10;
        if (value.startsWith("0x")) {
            base = 16;
            value = value.substring(2);
        }
        try {
            return Integer.valueOf(value, base);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    private static String decodeImplementer(int implementer) {
        // From "ETMIDR bit assignments".
        // http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ihi0014q/Bcfihfdj.html
        Log.e("IMPLEMENTER DECODE", String.valueOf(implementer));
        if (implementer == 0x41) {
            return "ARM";
        } else if (implementer == 0x44) {
            return "Digital Equipment Corporation";
        } else if (implementer == 0x4d) {
            return "Motorola";
        } else if (implementer == 0x51) {
            return "Qualcomm";
        } else if (implementer == 0x56) {
            return "Marvell";
        } else if (implementer == 0x69) {
            return "Intel";
        } else if (implementer == 0x65) {
            return "NVIDIA";
        } else {
            return "unknown (0x" + Integer.toHexString(implementer) + ")";
        }
    }

    public String getDeviceDetailsAsString(Activity context) {
        final StringBuilder result = new StringBuilder();
        cpu = new CPU();

        String[] procCpuLines = Utils.readFile("/proc/cpuinfo").split("\n");

        String processor = valueForKey(procCpuLines, "model name");
        if (processor == null) {
            processor = valueForKey(procCpuLines, "Processor");
        }

        cpu.setProcessorName(processor);
        int hardwareCoreCount = countHardwareCores();
        int enabledCoreCount = countEnabledCores();
        String cores = Integer.toString(hardwareCoreCount);
        if (enabledCoreCount != hardwareCoreCount) {
            cores += " (enabled: " + enabledCoreCount + ")";
        }
        cpu.setCore(cores);

        try {
            String minFrequency = Utils.readFile("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq").trim();
            String maxFrequency = Utils.readFile("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").trim();
            int minFrequencyHz = Integer.parseInt(minFrequency) * 1000;
            int maxFrequencyHz = Integer.parseInt(maxFrequency) * 1000;
            String prettyHz = Utils.prettyHz(maxFrequencyHz) + " idles at " + Utils.prettyHz(minFrequencyHz);
            cpu.setSpeed(prettyHz);
        } catch (Exception unexpected) {
            result.append("(Unable to determine CPU frequencies.)\n");
        }
        result.append('\n');

        // ARM-specific.
        int implementer = numericValueForKey(procCpuLines, "CPU implementer");
        if (implementer != -1) {
            cpu.setImplemeneter(decodeImplementer(implementer));
            cpu.setPart(decodePartNumber(numericValueForKey(procCpuLines, "CPU part")));
            // These two are included in the kernel's formatting of "Processor".
            cpu.setArquitecture(String.valueOf(numericValueForKey(procCpuLines, "CPU architecture")));
            cpu.setVariant(String.valueOf(numericValueForKey(procCpuLines, "CPU variant")));
            cpu.setHardware(valueForKey(procCpuLines, "Hardware"));
            cpu.setRevision(valueForKey(procCpuLines, "Revision"));
            cpu.setSerial(valueForKey(procCpuLines, "Serial"));
        }

        // MIPS-specific.
        // TODO: is "CPU architecture" ever more specific than "MIPS"?
        if ("MIPS".equals(valueForKey(procCpuLines, "CPU architecture"))) {
            cpu.setImplemeneter(valueForKey(procCpuLines, "CPU implementer"));
            cpu.setModel(valueForKey(procCpuLines, "cpu model"));
            cpu.setHardware(valueForKey(procCpuLines, "Hardware"));
            cpu.setRevision(valueForKey(procCpuLines, "Revision"));
            cpu.setSerial(valueForKey(procCpuLines, "Serial"));
        }

        // Intel-specific.
        String cacheSize = valueForKey(procCpuLines, "cache size");
        String addressSizes = valueForKey(procCpuLines, "address sizes");
        if (cacheSize != null) {
            cpu.setCache(cacheSize);
            cpu.setAddressSizes(addressSizes);
        }

        String features = valueForKey(procCpuLines, "Features");
        if (features == null) {
            features = valueForKey(procCpuLines, "flags");
        }
        cpu.setFeatures(Utils.sortedStringOfStrings(features.split(" ")));
        return result.toString();
    }

    private static String decodePartNumber(int part) {
        // TODO: if different implementers don't get discrete ranges,
        // we might need to test implementer here too.
        if (part == 0x920) {
            return "ARM920";
        } else if (part == 0x926) {
            return "ARM926";
        } else if (part == 0xa26) {
            return "ARM1026";
        } else if (part == 0xb02) {
            return "ARM11mpcore";
        } else if (part == 0xb36) {
            return "ARM1136";
        } else if (part == 0xb56) {
            return "ARM1156";
        } else if (part == 0xb76) {
            return "ARM1176";
        } else if (part == 0xc05) {
            return "Cortex-A5";
        } else if (part == 0xc07) {
            return "Cortex-A7";
        } else if (part == 0xc08) {
            return "Cortex-A8";
        } else if (part == 0xc09) {
            return "Cortex-A9";
        } else if (part == 0xc0f) {
            return "Cortex-A15";
        } else if (part == 0xc14) {
            return "Cortex-R4";
        } else if (part == 0xc15) {
            return "Cortex-R5";
        } else if (part == 0xc20) {
            return "Cortex-M0";
        } else if (part == 0xc21) {
            return "Cortex-M1";
        } else if (part == 0xc23) {
            return "Cortex-M3";
        } else if (part == 0xc24) {
            return "Cortex-M4";
        } else if (part == 0x00f) {
            return "Snapdragon S1 (Scorpion)";
        } else if (part == 0x02d) {
            return "Snapdragon S3 (Scorpion)";
        } else if (part == 0x04d) {
            return "Snapdragon S4 Plus (Krait)";
        } else if (part == 0x06f) {
            return "Snapdragon S4 Pro (Krait)";
        } else {
            return "unknown (0x" + Integer.toHexString(part) + ")";
        }
    }

    private void getProcessor() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Processor");
        cpuInfo.setValue(cpu.getProcessorName());
        listDeviceInfo.add(cpuInfo);
    }

    private void getFeatures() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Features");
        cpuInfo.setValue(cpu.getFeatures());
        listDeviceInfo.add(cpuInfo);
    }

    private void getCores() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Cores");
        cpuInfo.setValue(cpu.getCore());
        listDeviceInfo.add(cpuInfo);
    }

    private String getModel() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Model");
        cpuInfo.setValue(cpu.getModel());
        if (cpuInfo.getValue() != null) {
            listDeviceInfo.add(cpuInfo);
        }
        return cpu.getModel();
    }

    private void getArquitecture() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Arquitecture");
        cpuInfo.setValue(cpu.getArquitecture());
        listDeviceInfo.add(cpuInfo);
    }

    private void getPart() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Part");
        cpuInfo.setValue(cpu.getPart());
        listDeviceInfo.add(cpuInfo);
    }

    private void getSpeed() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Speed");
        cpuInfo.setValue(cpu.getSpeed());
        listDeviceInfo.add(cpuInfo);
    }

    private void getImplementer() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Implementer");
        cpuInfo.setValue(cpu.getImplemeneter());
        listDeviceInfo.add(cpuInfo);
    }

    private void getCache() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Cache");
        cpuInfo.setValue(cpu.getCache());
        if (cpuInfo.getValue() != null) {
            listDeviceInfo.add(cpuInfo);
        }
    }

    private void getSerial() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Serial");
        cpuInfo.setValue(cpu.getSerial());
        if (cpuInfo.getValue() != null) {
            listDeviceInfo.add(cpuInfo);
        }
    }

    private void getHardware() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Hardware");
        cpuInfo.setValue(cpu.getHardware());
        if (cpuInfo.getValue() != null) {
            listDeviceInfo.add(cpuInfo);
        }
    }

    private void getRevision() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Revision");
        cpuInfo.setValue(cpu.getRevision());
        if (cpuInfo.getValue() != null) {
            listDeviceInfo.add(cpuInfo);
        }
    }

    private void getAddressSize() {
        ListDeviceInfo cpuInfo = new ListDeviceInfo();
        cpuInfo.setTitle("Address Size");
        cpuInfo.setValue(cpu.getAddressSizes());
        if (cpuInfo.getValue() != null) {
            listDeviceInfo.add(cpuInfo);
        }
    }
}
