package com.salapp.phoneinfo.android.hardware.process;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.salapp.phoneinfo.MainActivity;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 2/15/13
 * Time: 4:02 PM
 */

public class FileUtils {
    private static final String PATH = Environment.getExternalStorageDirectory() + "/gpu_info.txt";
    private Map<Integer, String> dataConf = new HashMap<Integer, String>();
    private List<ListDeviceInfo> lstInfoGPU;

    public List<ListDeviceInfo> getGPUProcess() {
        this.checkIfFileExists();
        lstInfoGPU = new ArrayList<ListDeviceInfo>();
        getInfoGPU();
        return lstInfoGPU;
    }


    private List<ListDeviceInfo> getInfoGPU() {

        for (Map.Entry<Integer, String> integerStringEntry : dataConf.entrySet()) {
            Map.Entry maps = (Map.Entry) integerStringEntry;
            if (maps.getKey().equals(0)) {
                ListDeviceInfo listDeviceInfo = new ListDeviceInfo();
                listDeviceInfo.setTitle("Vendor");
                listDeviceInfo.setValue((String) maps.getValue());
                lstInfoGPU.add(listDeviceInfo);
            } else if (maps.getKey().equals(1)) {
                ListDeviceInfo listDeviceInfo = new ListDeviceInfo();
                listDeviceInfo.setTitle("Render");
                listDeviceInfo.setValue((String) maps.getValue());
                lstInfoGPU.add(listDeviceInfo);
            } else if (maps.getKey().equals(2)) {
                ListDeviceInfo listDeviceInfo = new ListDeviceInfo();
                listDeviceInfo.setTitle("Version");
                listDeviceInfo.setValue((String) maps.getValue());
                lstInfoGPU.add(listDeviceInfo);
            } else if (maps.getKey().equals(3)) {
                ListDeviceInfo listDeviceInfo = new ListDeviceInfo();
                listDeviceInfo.setTitle("Extension");
                listDeviceInfo.setValue((String) maps.getValue());
                lstInfoGPU.add(listDeviceInfo);
            }
        }
        return lstInfoGPU;
    }

    public void checkIfFileExists(){
        File file = new File(PATH);
        if(file.exists()){
            readFromFile();
        }else{
            Log.e("FILE_EXISTS", "File doesn't exists. Please check the file");
        }
    }

    public String readFromFile() {

        File file = new File(PATH);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                dataConf.put(count, line);
                count++;
            }

        } catch (IOException ioe) {
            Log.e("Error reading file", ioe.getMessage());
        } finally {
            try {
                assert br != null;
                if (br != null) {
                    br.close();
                }

            } catch (IOException ioe) {
                Log.e(ioe.getMessage(), ioe.getLocalizedMessage());
            }
        }
        return "";
    }

    //Write GPU Info into a file in the memory
    public void writeToFile(String info) {
        File file = new File(PATH);

        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(file));
            br.write(info);
            br.flush();
            br.close();
        } catch (IOException ioe) {
            Log.e("FILE_UTIL_TAG", ioe.getMessage());
        }
    }
}
