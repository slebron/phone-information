package com.salapp.phoneinfo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.android.hardware.process.FileUtils;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/4/13
 * Time: 9:58 AM
 */
public class GPUFragment extends Fragment {
    private ListView lstAndroidInformation;
    private List<ListDeviceInfo> lstInfoGPU;



    public GPUFragment(){
        lstInfoGPU = new ArrayList<ListDeviceInfo>();
        lstInfoGPU = new FileUtils().getGPUProcess();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gpu_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstAndroidInformation = (ListView) getView().findViewById(R.id.lstGPUInfo);
        lstAndroidInformation.setAdapter(new GPUAdapter(this));
    }

    private class GPUAdapter extends ArrayAdapter<ListDeviceInfo> {
        Activity context;

        public GPUAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstInfoGPU);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstInfoGPU.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstInfoGPU.get(position).getValue());
            return (view);
        }
    }
}
