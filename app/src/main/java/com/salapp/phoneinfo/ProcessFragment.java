package com.salapp.phoneinfo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import com.salapp.phoneinfo.android.hardware.process.ProcessManager;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/14/13
 * Time: 9:16 AM
 */
public class ProcessFragment extends Fragment{

    private List<ListDeviceInfo> lstProcessDevice;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.process_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = this.getActivity().getApplicationContext();
        lstProcessDevice = new ProcessManager(this).getAllProcess();

        ListView lstDeviceInformation = (ListView) getView().findViewById(R.id.lstProcessInfo);
        lstDeviceInformation.setAdapter(new ProcessAdapter(this));
    }

    private class ProcessAdapter extends ArrayAdapter<ListDeviceInfo> {
        private Activity context;

        ProcessAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstProcessDevice);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstProcessDevice.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstProcessDevice.get(position).getValue());

            return (view);
        }
    }
}
