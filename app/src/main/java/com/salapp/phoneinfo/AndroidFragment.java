package com.salapp.phoneinfo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.process.AndroidProcess;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/7/13
 * Time: 8:52 AM
 */
public class AndroidFragment extends Fragment {
    private ListView lstAndroidInformation;
    private List<ListDeviceInfo> lstInfoAndroid;


    public AndroidFragment() {
        lstInfoAndroid = new ArrayList<ListDeviceInfo>();
        lstInfoAndroid = new AndroidProcess().getAndroidInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.android_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstAndroidInformation = (ListView) getView().findViewById(R.id.lstAndroidInfo);
        lstAndroidInformation.setAdapter(new AndroidAdapter(this));
    }

    private class AndroidAdapter extends ArrayAdapter<ListDeviceInfo> {
        Activity context;

        public AndroidAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstInfoAndroid);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstInfoAndroid.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstInfoAndroid.get(position).getValue());
            return (view);
        }
    }
}
