package com.salapp.phoneinfo.permission;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

/**
 * @author stainley lebron
 * @since on 10/12/2016.
 */

public class AndroidPermission {

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasPermission(Context context, String... permissions) {
        if (shouldAskPermission() != false) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

}
