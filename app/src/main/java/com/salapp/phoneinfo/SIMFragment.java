package com.salapp.phoneinfo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/8/13
 * Time: 1:40 PM
 */
public class SIMFragment extends Fragment {
    private List<ListDeviceInfo> lstInfoDevice;

    private Context context = null;

    public SIMFragment() {
        lstInfoDevice = new ArrayList<ListDeviceInfo>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sim_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = this.getActivity().getApplicationContext();
        lstInfoDevice = new SIMProcess(this).getSIMProcess();

        ListView lstDeviceInformation = (ListView) getView().findViewById(R.id.lstSIMInfo);
        lstDeviceInformation.setAdapter(new SIMAdapter(this));
    }

    private class SIMAdapter extends ArrayAdapter<ListDeviceInfo> {
        private Activity context;

        SIMAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstInfoDevice);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstInfoDevice.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstInfoDevice.get(position).getValue());

            return (view);
        }
    }
}
