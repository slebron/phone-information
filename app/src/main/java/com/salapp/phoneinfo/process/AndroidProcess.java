package com.salapp.phoneinfo.process;

import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/7/13
 * Time: 9:07 AM
 */
public class AndroidProcess {
    private List<ListDeviceInfo> lstAndroidInfo;
    private ListDeviceInfo androidInfo;

    public List<ListDeviceInfo> getAndroidInfo() {
        lstAndroidInfo = new ArrayList<ListDeviceInfo>();
        getManufacturer();
        getModel();
        getProduct();
        getAndroidVersion();
        getAPIVersion();
        getCodename();
        getBoard();
        return lstAndroidInfo;
    }

    public void getAndroidVersion() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("Android Version");
        androidInfo.setValue(Build.VERSION.RELEASE);
        lstAndroidInfo.add(androidInfo);
    }

    private void getProduct() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("Product");
        androidInfo.setValue(Build.PRODUCT);
        lstAndroidInfo.add(androidInfo);
    }

    private void getModel() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("Model");
        androidInfo.setValue(Build.MODEL);
        lstAndroidInfo.add(androidInfo);
    }

    public void getBoard() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("Board");
        androidInfo.setValue(Build.BOARD);
        lstAndroidInfo.add(androidInfo);
    }

    private void getManufacturer() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("Manufacturer");
        androidInfo.setValue(Build.MANUFACTURER);
        lstAndroidInfo.add(androidInfo);
    }

    public void getAPIVersion() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("API Version");
        androidInfo.setValue(String.valueOf(Build.VERSION.SDK_INT));
        lstAndroidInfo.add(androidInfo);
    }

    public void getCodename() {
        androidInfo = new ListDeviceInfo();
        androidInfo.setTitle("Code name");
        switch (Build.VERSION.SDK_INT) {
            case 1:
                androidInfo.setValue("Apple Pie");
                break;
            case 2:
                androidInfo.setValue("Banana Bread");
                break;
            case 3:
                androidInfo.setValue("Cupcake");
                break;
            case 4:
                androidInfo.setValue("Donut");
                break;
            case 5:
            case 6:
            case 7:
                androidInfo.setValue("Eclair");
                break;
            case 8:
                androidInfo.setValue("Froyo");
                break;
            case 9:
            case 10:
                androidInfo.setValue("Gingerbread");
                break;
            case 11:
            case 12:
            case 13:
                androidInfo.setValue("Honeycomb");
                break;
            case 14:
            case 15:
                androidInfo.setValue("Ice Cream Sandwich");
                break;
            case 16:
            case 17:
            case 18:
                androidInfo.setValue("Jelly Bean");
                break;

            case 19:
                androidInfo.setValue("Kit Kat");
                break;
            case 21:
            case 22:
                androidInfo.setValue("Lollipop");
                break;
            case 23:
                androidInfo.setValue("Marshmallow");
                break;
            case 24:
                androidInfo.setValue("Nougat");
                break;
            default:
        }
        lstAndroidInfo.add(androidInfo);
    }
}
