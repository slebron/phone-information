package com.salapp.phoneinfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.salapp.phoneinfo.android.hardware.process.GPUProcess;
import com.salapp.phoneinfo.permission.AndroidPermission;


/**
 * User: Stainley Lebron
 * Date: 5/12/13
 * Time: 5:22 PM
 */
public class PhoneInfo extends Activity {
    private final static int REQUEST_CODE_ASK_PERMISSION = 1;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setPermission();
    }


    //If permission is invoked first time
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSION:
                // Check Permissions Granted or not
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //new ContactSyncTask().execute();
                    pDialog = ProgressDialog.show(this, "Scanning Phone....", "Please wait", true, false);
                    pDialog.onStart();

                    GLSurfaceView surfaceView = new GLSurfaceView(this);
                    surfaceView.setRenderer(new GPUProcess());
                    setContentView(surfaceView);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(PhoneInfo.this, MainActivity.class);
                            pDialog.dismiss();
                            startActivity(intent);
                        }
                    }, 800);

                } else {
                    // Permission Denied
                    Toast.makeText(this, "Read contact permission is denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void setPermission() {

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_PHONE_STATE
                , android.Manifest.permission.INTERNET,
                android.Manifest.permission.ACCESS_NETWORK_STATE
        };

        if (!AndroidPermission.hasPermission(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            pDialog = ProgressDialog.show(this, "Scanning Phone....", "Please wait", true, false);
            pDialog.onStart();

            GLSurfaceView surfaceView = new GLSurfaceView(this);
            surfaceView.setRenderer(new GPUProcess());
            setContentView(surfaceView);

            Intent intent = new Intent(PhoneInfo.this, MainActivity.class);
            pDialog.dismiss();
            startActivity(intent);
        }
    }
}