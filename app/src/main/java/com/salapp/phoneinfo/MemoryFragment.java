package com.salapp.phoneinfo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.android.hardware.process.MemoryProcess;
import com.salapp.phoneinfo.vo.ListDeviceInfo;

/**
 * User: Stainley Lebron
 * Date: 5/12/13
 * Time: 9:45 PM
 */
public class MemoryFragment extends Fragment {
    private List<ListDeviceInfo> lstMemoryInfo;
    private ListView lstMemoryInformation;
    private Context context;

    public MemoryFragment(){
        lstMemoryInfo = new ArrayList<ListDeviceInfo>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.memory_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = this.getActivity().getApplicationContext();
        lstMemoryInfo = new MemoryProcess(this).getInfoMemory();

        lstMemoryInformation = (ListView) getView().findViewById(R.id.lstMemoryInfo);
        lstMemoryInformation.setAdapter(new CPUAdapter(this));
    }

    private class CPUAdapter extends ArrayAdapter<ListDeviceInfo> {
        Activity context;

        public CPUAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstMemoryInfo);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstMemoryInfo.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstMemoryInfo.get(position).getValue());
            return (view);
        }
    }

}
