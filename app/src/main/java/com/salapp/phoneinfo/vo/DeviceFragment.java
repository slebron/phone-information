package com.salapp.phoneinfo.vo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.salapp.phoneinfo.R;
import com.salapp.phoneinfo.android.hardware.process.DeviceProcess;

/**
 * User: Stainley Lebron
 * Date: 5/4/13
 * Time: 11:28 AM
 */
public class DeviceFragment extends Fragment {
    private ListView lstDeviceInformation;
    private List<ListDeviceInfo> lstInfoDevice;

    public DeviceFragment() {
        lstInfoDevice = new ArrayList<ListDeviceInfo>();
        lstInfoDevice = new DeviceProcess().getDeviceProcess();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.device_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstDeviceInformation = (ListView) getView().findViewById(R.id.lstDeviceInfo);
        lstDeviceInformation.setAdapter(new DeviceAdapter(this));
    }

    private class DeviceAdapter extends ArrayAdapter<ListDeviceInfo> {

        Activity context;

        DeviceAdapter(Fragment context) {
            super(context.getActivity(), R.layout.list_item_devices, lstInfoDevice);
            this.context = context.getActivity();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View view = inflater.inflate(R.layout.list_item_devices, null);

            TextView title = (TextView) view.findViewById(R.id.lblDe);
            title.setText(lstInfoDevice.get(position).getTitle());

            TextView value = (TextView) view.findViewById(R.id.lblAsunto);
            value.setText(lstInfoDevice.get(position).getValue());

            return (view);
        }
    }
}
