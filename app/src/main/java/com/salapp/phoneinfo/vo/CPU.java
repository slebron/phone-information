package com.salapp.phoneinfo.vo;

/**
 * User: Stainley Lebron
 * Date: 5/17/13
 * Time: 8:44 AM
 */
public class CPU {
    private String processorName;
    private String features;
    private String part;
    private String speed;
    private String variant;
    private String revision;
    private String hardware;
    private String implemeneter;
    private String serial;
    private String core;
    private String arquitecture;
    private String model;
    private String cache;
    private String addressSizes;

    public String getProcessorName() {
        return processorName;
    }

    public void setProcessorName(String processorName) {
        this.processorName = processorName;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getImplemeneter() {
        return implemeneter;
    }

    public void setImplemeneter(String implemeneter) {
        this.implemeneter = implemeneter;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getCore() {
        return core;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public String getArquitecture() {
        return arquitecture;
    }

    public void setArquitecture(String arquitecture) {
        this.arquitecture = arquitecture;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCache() {
        return cache;
    }

    public void setCache(String cache) {
        this.cache = cache;
    }

    public String getAddressSizes() {
        return addressSizes;
    }

    public void setAddressSizes(String addressSizes) {
        this.addressSizes = addressSizes;
    }
}
