package com.salapp.phoneinfo.vo;

/**
 * User: Stainley Lebron
 * Date: 5/6/13
 * Time: 10:34 AM
 */

public class ListDeviceInfo {
    private String title;
    private String value;

    public ListDeviceInfo() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
