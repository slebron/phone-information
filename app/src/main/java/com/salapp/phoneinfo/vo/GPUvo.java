package salapp.com.phoneinfo.vo;

import java.io.Serializable;

/**
 * User: Stainley Lebron
 * Date: 5/4/13
 * Time: 9:46 AM
 */
public class GPUvo implements Serializable {
    private String gpuVendor;
    private String modelGPU;

    public GPUvo() {
        this.gpuVendor = "NVIDIA";
        this.modelGPU = "GTX 230";
    }

    public String getGpuVendor() {
        return gpuVendor;
    }

    public void setGpuVendor(String gpuVendor) {
        this.gpuVendor = gpuVendor;
    }

    public String getModelGPU() {
        return modelGPU;
    }

    public void setModelGPU(String modelGPU) {
        this.modelGPU = modelGPU;
    }

    public GPUvo getGPU(){
        return new GPUvo();
    }
}
