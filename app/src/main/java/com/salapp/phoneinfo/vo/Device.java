package com.salapp.phoneinfo.vo;

/**
 * User: Stainley Lebron
 * Date: 5/4/13
 * Time: 11:22 AM
 */
public class Device {
    private String model;
    private String brand;
    private String serial;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
