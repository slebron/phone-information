package com.salapp.phoneinfo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static int step = 0;
    private InterstitialAd interstitialAd;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-7748550073260719/2672231781");

        //Create Ad Request
        AdRequest adr = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        //Begin loading  your interstitial
        interstitialAd.loadAd(adr);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    private void showInterstitialAds() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    AdRequest adRequest = new AdRequest.Builder().build();
                    interstitialAd.loadAd(adRequest);
                    super.onAdClosed();
                }
            });
        }
    }

    private void counter(int value) {
        //Show interstitial
        if (value == 4) {
            this.showInterstitialAds();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_share:
                comparte();
                break;
            case R.id.action_rating:
                rankeame();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment fragment1;
            Bundle args = new Bundle();

            switch (position) {
                case 0:
                    step += 1;
                    fragment1 = new AndroidFragment();
                    args.putString("android_fragment", "android");
                    break;

                case 1:
                    step += 1;
                    fragment1 = new DisplayFragment();
                    args.putString("display_fragment", "display_fragment");
                    break;
                case 2:
                    step += 1;
                    fragment1 = new CPUFragment();
                    args.putString("cpu_info", "cpu_info");
                    break;
                case 3:
                    step += 1;
                    fragment1 = new SIMFragment();
                    args.putString("sim_fragment", "sim_fragment");
                    break;
                case 4:
                    step += 1;
                    fragment1 = new MemoryFragment();
                    args.putString("memory_info", "memory_info");
                    break;
                case 5:
                    step += 1;
                    fragment1 = new ProcessFragment();
                    args.putString("proc_fragment", "process_fragment");
                    break;
                case 6:

                    step += 1;
                    fragment1 = new GPUFragment();
                    args.putString("gpu_info", "gpu_info");
                    break;

                default:
                    step += 1;
                    fragment1 = new AndroidFragment();
                    args.putString("android_fragment", "android");
            }
            if (step >= 4) {
                counter(step);
                step = 0;
            }

            fragment1.setArguments(args);
            return fragment1;
        }

        @Override
        public int getCount() {
            // Show 7 total pages.
            return 7;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale locale = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_android).toUpperCase(locale);
                case 1:
                    return getString(R.string.title_display).toUpperCase(locale);

                case 2:
                    return getString(R.string.title_cpu).toUpperCase(locale);
                case 3:
                    return getString(R.string.title_sim).toUpperCase(locale);
                case 4:
                    return getString(R.string.title_memory).toUpperCase(locale);
                case 5:
                    return getString(R.string.title_process).toUpperCase(locale);
                case 6:
                    return getString(R.string.title_gpu).toUpperCase(locale);
            }
            return null;
        }
    }

    public void comparte() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "This application let you see all of your device's internal information in one place, easy and simple. Check your running processes, carrier information, memory usage and much more!> Download  Phone Information on Google Play. -> https://play.google.com/store/apps/details?id=com.salapp.phoneinfo";
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Download Phone Information"));

    }

    public void rankeame() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.salapp.phoneinfo"));
        this.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        /*if (adView != null) {
            adView.destroy();
        }*/
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        /*adView.pause();*/
        super.onPause();
    }

    @Override
    protected void onResume() {
        /*adView.resume();*/
        super.onResume();
    }
}
